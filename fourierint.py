import numpy as n

from scipy.integrate import quad

def fourierint_1d(integrand, a, b, r, isreal=True, inverse=False, **kwargs):
    """A 1-d Fourier integral.  The integrand f(s) is multiplied by
    exp(-2pi i r s), where s is the independent variable.  s is
    integrated over the range (a, b).  If the inverse keyword is set,
    the argumet of the exponential is (+2pi i r s).

    """
    wvar = -2.*n.pi*r
    if inverse: wvar *= -1.
    
    if isreal:
        pr, er = quad(integrand, a, b, weight='cos', wvar=wvar, **kwargs)
        pi, ei = quad(integrand, a, b, weight='sin', wvar=wvar, **kwargs)
        return pr + 1j*pi, er+1j*ei

    real_integrand = lambda x: integrand(x).real
    imag_integrand = lambda x: integrand(x).imag

    cr, cre = quad(real_integrand, a, b, weight='cos', wvar=wvar, **kwargs)
    sr, sre = quad(real_integrand, a, b, weight='sin', wvar=wvar, **kwargs)
    ci, cie = quad(imag_integrand, a, b, weight='cos', wvar=wvar, **kwargs)
    si, sie = quad(imag_integrand, a, b, weight='sin', wvar=wvar, **kwargs)

    # combine real and imaginary parts
    r = cr-si
    i = sr+ci
    v = r + 1j*i

    # errors add in quadrature
    re = n.sqrt(cre**2 + sie**2)
    ie = n.sqrt(sre**2 + cie**2)
    e = re + 1j*ie

    return v, e


def fourierint_2d(integrand, a, b, c, d, ry, rx, isreal=True, **kwargs):
    """A 2-d Fourier integral.  The integrand f(s) is multiplied by
    exp(-2pi i r.s), where s is the 2d independent variable.  s is
    integrated over the range (a, b) in the first coordinate, and (c,
    d) in the second.  If the inverse keyword is set, the argumet of
    the exponential is (+2pi i r.s).

    """
    # NOTE  errors are ignored...
    def integrandy(y):
        integrandx = lambda x: integrand(y,x)
        v, e = fourierint_1d(integrandx, c, d, rx, isreal=isreal, **kwargs)
        return v
    v, e = fourierint_1d(integrandy, a, b, ry, isreal=False, **kwargs)
    return v, e
