# pyfitz repository of useful routines from Michael Fitzgerald

* `per_spline.py` routines for computing periodic splines
* `bhmie.py` Bohren-Huffman Mie scattering
* `fourierint.py` Routines for computing Fourier integrals.
* `image.py` Image-processing routines, including rotation/scaling, combining multime images, radial profile calculation, cosmic-ray rejection.
* `kepler.py` Code for computing Keplerian orbits
* `poly.py` Manipulating 2d polynomials



