# code for manipulating 2d polynomials
# Michael Fitzgerald (mpfitz@ucla.edu)

import numpy as n


def get_n_coeff(deg):
    "return the number of coefficients for a 2d polynomial of a certain degree"
    return ((2*deg+3)**2-1)//8

def get_degree(n_coeff):
    "return the degree given the number of coefficients for a 2d polynomial"
    return int((n.sqrt(1+8*n_coeff)-3)/2.)


def get_ca(a):
    "compute coefficient array (scalar result) for a 2d polynomial"

    # get polynomial degree
    deg = get_degree(len(a))
    #print deg, deg*(deg+1)/2, len(a)

    # convert coefficients to format numpy likes
    ca = n.zeros((deg+1, deg+1), dtype=n.float)
    j = 0
    for k in range(deg+1):
        for i in range(k+1):
            #print i, k, j
            ca[i,k-i] = a[j]
            j += 1

    return ca


def get_ca_cb(a, b):
    "compute a and b coefficient arrays (vector result) for 2d polynomials"

    # get polynomial degree
    assert len(a) == len(b)
    deg = get_degree(len(a))
    #print deg, deg*(deg+1)/2, len(a)

    # convert coefficients to format numpy likes
    ca = n.zeros((deg+1, deg+1), dtype=n.float)
    cb = n.zeros((deg+1, deg+1), dtype=n.float)
    j = 0
    for k in range(deg+1):
        for i in range(k+1):
            #print i, k, j
            ca[i,k-i] = a[j]
            cb[i,k-i] = b[j]
            j += 1

    return ca, cb


def forward_2d_poly(x, y, a):
    """
    Compute u = f(x,y) with coefficient array a.

    FIXME  check this is correct:
    u = a_0 + a_1*x + a_2*y + a_3*x*y + a_4*x^2 + a^5*y^2 + ...
    """

    ca = get_ca(a)

    # evaluate polynomial
    from numpy.polynomial.polynomial import polyval2d
    u = polyval2d(y, x, ca)

    return u


def forward_2d_poly_vec(x, y, a, b):
    """
    Compute (u,v) = f(x,y) with coefficient arrays a, b.

    FIXME  check this is correct:
    u = a_0 + a_1*x + a_2*y + a_3*x*y + a_4*x^2 + a^5*y^2 + ...
    v = b_0 + b_1*x + b_2*y + b_3*x*y + b_4*x^2 + b^5*y^2 + ...
    """

    ca, cb = get_ca_cb(a, b)

    # evaluate polynomial
    from numpy.polynomial.polynomial import polyval2d
    u = polyval2d(y, x, ca)
    v = polyval2d(y, x, cb)

    return u, v


def inverse_2d_poly_vec(u, v, a, b):
    """
    Compute (x,y) = f^{-1}(u,v) with coefficient arrays a, b.
    """

    # pack values into 1d parameter array
    get_p = lambda x, y: n.array((x,y)).flatten()

    def get_xy(p):
        "unpack parameters to values"
        x, y = p[0:len(p)/2], p[len(p)/2:]
        x.shape = u.shape
        y.shape = v.shape
        return x, y

    from scipy.optimize import leastsq

    sp = get_p(u.copy(), v.copy()) # starting parameters

    def fit_fn(p):
        x, y = get_xy(p)
        up, vp = forward_2d_poly(x, y, a, b)
        return n.array((u-up, v-vp)).flatten()

    # perform least-squares fit
    p_opt, ier = leastsq(fit_fn, sp.copy())
    assert ier in (1, 2, 3, 4)

    return get_xy(p_opt)
