#!/usr/bin/env python
#
# Michael Fitzgerald (mpfitz@ucla.edu) 2014-2-5
#
# code for periodic splines
#

import numpy as n
import matplotlib as mpl
import pylab

import logging
_log = logging.getLogger('per_spline')


def test_spline():
    from scipy.interpolate import splrep, splev

    # compute a test set of data points
    x = n.linspace(0., 2.*n.pi, 10)
    y = n.sin(x)

    # get spline interpolator info
    t, c, k = splrep(x, y)

    # get spline interpolation
    xx = n.linspace(0., 2.*n.pi, 100)
    yy = splev(xx, (t,c,k))

    # show results
    fig = pylab.figure(0)
    fig.clear()
    ax = fig.add_subplot(111)

    ax.scatter(x, y, c='k')
    ax.plot(xx, yy, c='b')
    ax.scatter(t, c, c='r')

    pylab.draw()
    pylab.show()

    #ipshell() # TEMP



def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]

    return t, c, k

def get_tck2(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:k] = params[-k:]
    c[k:n_param+k] = params

    return t, c, k



def get_tck180(p, k=3):
    "get periodic spline parameters for mirrored past 180deg"

    # mirror first cell
    ik = int(n.ceil(k/2.))
    params = n.concatenate((p[0:ik], p[k-ik-1::-1], p[ik:]))

    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/(2*n_param-k-1)

    n_knot = 2*n_param-1 + k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:2*n_param-1] = params[-2::-1]

    return t, c, k

    

from scipy.interpolate import splev
eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)

def model_fn(phi, params, **kwargs):
    tck = get_tck(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn2(phi, params, **kwargs):
    tck = get_tck2(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn180(phi, params, **kwargs):
    tck = get_tck180(params, **kwargs)
    return eval_pspl(phi, tck)




def show_spline(t, c, k, fignum=0):

    nu = n.linspace(t.min(), t.max(), 100)

    fig = pylab.figure(fignum)
    fig.clear()
    ax = fig.add_subplot(111)

    nn = len(t)
    assert len(t) == len(c)

    colors = mpl.rcParams['axes.prop_cycle'].by_key()['color']
    n_c = len(colors)

    for i in range(nn):
        cp = n.zeros_like(c)
        cp[i] = c[i]
        v = splev(nu, (t, cp, k))
        ax.plot(nu, v,
                c=colors[i % n_c],
                ls='--',
                )
        j = n.argmax(v)
        ax.plot((t[i], nu[j]),
                (c[i], v[j]),
                c=colors[i % n_c],
                ls=':',
                )
                

    v = splev(nu, (t, c, k))
    ax.plot(nu, v, c='k', ls='-')
    v2 = splev(nu % (2.*n.pi), (t, c, k))
    ax.plot(nu, v2, c='k', ls='-.')
    ax.scatter(t, c, c='r')

    w = n.nonzero((nu>=0.) & (nu <= 2.*n.pi))[0]
    ax.set_ylim(n.min((v[w].min(), c.min(), 0.)),
                n.max((v[w].max(), c.max())),
                )
    ax.set_autoscale_on(False)
    for loc in (0., n.pi, 2.*n.pi):
        ax.axvline(loc, c='k')

    pylab.draw()
    pylab.show()
    


def test_pspl_model(test_180=False):

    k = 3
    if test_180:
        n_d = 6
        n_p = 6
    else:
        n_d = 8
        n_p = 8
    n_d2 = 10*n_d

    rs = n.random.RandomState(seed=2345)
    if test_180:
        phi = n.linspace(0., n.pi, n_d, endpoint=True)
        r = 3. + rs.randn(n_d)*.4
    else:
        phi = n.linspace(0., 2.*n.pi, n_d, endpoint=False)
        r = 3. + rs.randn(n_d)*.4
        ## r = 3. + rs.randn(n_d/2+1)*.4
        ## r = n.concatenate((r, r[-2:0:-1]))
    x, y = r*n.cos(phi), r*n.sin(phi)

    if test_180:
        mod_fn = model_fn180
        tck_fn = get_tck180
    else:
        mod_fn = model_fn
        tck_fn = get_tck


    from scipy.optimize import leastsq
    def fit_fn(p):
        return r - mod_fn(phi, p, k=k)
    sp = 3.*n.ones(n_p, dtype=n.float)
    p_opt, cov, infodict, msg, ier = leastsq(fit_fn,
                                             sp.copy(),
                                             full_output=True,
                                             )
    print(ier, msg)



    t, c, k = tck_fn(p_opt, k=k)
    ## print("dt = {:f}".format(t[1]-t[0]))

    tx, ty = c*n.cos(t), c*n.sin(t)

    phi2 = n.linspace(0., 2.*n.pi, n_d2)
    rr = mod_fn(phi2, p_opt)
    xx, yy = rr*n.cos(phi2), rr*n.sin(phi2)


    # show results
    fig = pylab.figure(0)
    fig.clear()

    ax = fig.add_subplot(111)

    ax.scatter(x, y, c='k')
    ax.plot(xx, yy, c='b')

    w = n.nonzero(c)
    ax.plot(tx[w], ty[w], c='r', marker='o')

    ax.set_aspect('equal')

    pylab.draw()
    pylab.show()


    fignum = 2 if test_180 else 1
    show_spline(t, c, k, fignum=fignum)


    #ipshell()
    


if __name__=='__main__':
    test_spline()
    test_pspl_model()
    test_pspl_model(test_180=True)

